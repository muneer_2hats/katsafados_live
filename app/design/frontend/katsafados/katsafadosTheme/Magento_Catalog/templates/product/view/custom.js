require(['jquery', 'jquery/ui'], function ($) {
    jQuery(document).ready(function () {

        jQuery("li.external-links").hover(function () {
            jQuery('.external-link-ul').css("display", "block");
        }, function () {
            jQuery(".external-link-ul").hover(function () {
                jQuery('.external-link-ul').css("display", "block");
            }, function () {
                jQuery(this).css("display", "none");
            });
        });

        jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = jQuery(e.target);
            var targetText = jQuery(target).text() // activated tab
            jQuery(target).closest('.nav-tabs').attr("title", targetText)
        });

    });


    var closed = false;
    var executeOnce = false;

    function addCloseButtonOnTopNav() {
        if (jQuery(window).width() < 1200) {

            if (!executeOnce) {
                executeOnce = true;
                jQuery(".top-categories-list > h4").click(function () {
                    jQuery(this).siblings(".sub-cat-ul").slideToggle();
                });

                jQuery(".left-tabs-wrap h4").click(function () {
                    jQuery(this).siblings(".nav-tabs").slideToggle();
                });

                jQuery(".catalog-category-view .title,.catalog-category-view dt").click(function () {
                    jQuery(".sidebar-main .items").slideToggle();
                });

                jQuery(".catalog-category-view .filter-title").click(function () {
                    jQuery(".sidebar-main .filter-content").slideToggle();
                });

            }

            if (!(jQuery(".header.panel .header.links>ul .close-btn").length)) {

                if (!closed) {
                    var ph = jQuery(".header.panel .contactus-link a").attr("href").match(/\d+/);
                    jQuery(".header.panel .header.links").append(jQuery(".header.panel .contactus-link").clone());
                    jQuery(".header.panel .contactus-link a").text(ph);
                }

                jQuery(".header.panel .header.links>ul").append("<div class='close-sec'><button class='close-btn'>X Close</button></div>");
            }
            jQuery(".header.panel .header.links>ul .close-btn").on("click", function () {
                jQuery(".header.panel .header.links>ul").remove();
                jQuery(".header.panel .header.links").addClass("closed");
                closed = true;
            });
        } else {
            jQuery(".header.panel .header.links > .contactus-link").remove();
            executeOnce = false;
        }

        if (jQuery(window).width() < 767) {

            jQuery(".catalog-category-view .title,.catalog-category-view dt").click(function () {
                jQuery(".sidebar-main .items").slideToggle();
            });

            jQuery(".catalog-category-view .sidebar-main .filter .block-subtitle").click(function () {
                jQuery(".sidebar-main .filter-options").slideToggle();
            });

            jQuery("html").addClass("nav-open");

        }
        jQuery(".header.panel .header.links>ul .close-btn").on("click", function () {
            jQuery(".header.panel .header.links>ul").remove();
        });
    }

    addCloseButtonOnTopNav();

    jQuery(window).resize(function () {
        addCloseButtonOnTopNav();
    });

    jQuery(document).on('click', '.qty button', function () {
        var qty = jQuery('#qty').val();
        var cartPageQty = jQuery(this).siblings(".input-text").val();
        if (jQuery(this).hasClass('sub')) {
            qty--;
            cartPageQty--;
        }
        if (jQuery(this).hasClass('add')) {
            qty++;
            cartPageQty++;
        }
        jQuery('#qty').val(qty);
        jQuery(this).siblings(".input-text").val(cartPageQty)
    });

    // detail page read more 
    var maxLength = 100;
    jQuery(".catalog-product-view .product-info-main .product.attribute.overview p").each(function(){
        var myStr = jQuery(this).text();
        if(jQuery.trim(myStr).length > maxLength){
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, jQuery.trim(myStr).length);
            jQuery(this).empty().html(newStr);
            jQuery(this).append(' <a href="javascript:void(0);" class="read-more">...<span>Read More</span></a>');
            jQuery(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    jQuery(".read-more").click(function(){
        jQuery(this).siblings(".more-text").contents().unwrap();
        jQuery(this).remove();
    });

});
