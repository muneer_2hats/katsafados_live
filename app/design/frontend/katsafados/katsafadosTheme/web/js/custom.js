require(['jquery', 'jquery/ui', 'slick'], function(jQuery) {
    var $ = jQuery.noConflict();
    jQuery(document).ready(function() {
        jQuery('body').css('pointer-events', 'all');

        if (jQuery(".category-view  .category-description").length) {
            jQuery(".category-view  .category-description").prependTo("#maincontent .columns")
            var categoryDescriptionHeight = jQuery("#maincontent .columns  .category-description").height();
            jQuery("body").get(0).style.setProperty("--categoryDescriptionHeight", categoryDescriptionHeight + 'px');
        }

        /* navbar toggle */
        jQuery('html .sections.nav-sections').attr('id', 'mainNav');

        //add class to switcher if lang is greek
        if (jQuery("body").hasClass("greek"))
            jQuery(".switcher-language").addClass("greek");

        //add class to category list page toolbar
        if (jQuery(".catalog-category-view .column.main .sub-cat").length)
            jQuery(".toolbar-products").addClass("category-included")

        jQuery('html').click(function(e) {
            var inSideDiv = (e.target.id == "mainNav" || jQuery(e.target).parents("#mainNav").length);
            if (!inSideDiv && jQuery("html .sections.nav-sections").hasClass(" nav-before-open showNav nav-open")) {
                jQuery("html .sections.nav-sections").toggleClass(" nav-before-open showNav nav-open");
            }
        });

        jQuery('html').on("click", ".nav-toggle", function(event) {
            jQuery("html").toggleClass(" nav-before-open showNav nav-open");
            event.stopPropagation();
        });
        /* navbar toggle ends */


        /* top panel dropdowns */


        jQuery('.dropdown').hover(function() {
            jQuery('.dropdown-toggle', this).trigger('click');
        });


        if (jQuery(window).width() > 1199) {

            jQuery(".authorization-link").mouseenter(function() {
                jQuery(".customer-welcome,.customer-name").toggleClass("active");
            });

            jQuery(".customer-welcome .customer-menu ul").mouseleave(function() {
                jQuery(".customer-welcome,.customer-name").toggleClass("active");
            });

            jQuery("#switcher-language .switcher-trigger").mouseenter(function() {
                jQuery(this).addClass("active");
                jQuery(this).parent().addClass("active");
            });

            jQuery("#switcher-language .switcher-trigger").mouseleave(function() {
                jQuery(this).removeClass("active");
                jQuery(this).parent().removeClass("active");
            });

            jQuery("#switcher-language .switcher-dropdown").mouseleave(function() {
                jQuery(this).toggleClass("active");
                jQuery(this).parent().toggleClass("active");
            });

            jQuery("li.external-links").hover(function() {
                jQuery('.external-link-ul').css("display", "block");
            }, function() {
                jQuery(".external-link-ul").hover(function() {
                    jQuery('.external-link-ul').css("display", "block");
                }, function() {
                    jQuery(this).css("display", "none");
                });
            });
        }


        /* top panel dropdowns ends */

        /* hover tab */

        jQuery('.nav-tabs a').hover(function(e) {
            e.preventDefault();
            jQuery('.tab-pane').removeClass('active');
            tabContentSelector = jQuery(this).attr('href');
            jQuery(this).tab('show');
            jQuery(tabContentSelector).addClass('active');
        });
        /* hovertab ends */

        // product listing page sidebar dropdown show on desktop  

        if (jQuery(window).width() > 1199) {
            jQuery(".catalog-category-view .filter-options .filter-options-item .filter-options-content").attr('style', 'display: block !important');
        }

        // custom toggle mobile navbar 



        //add class to body when customer logged in
        if (jQuery('.authorization-link > a').length) {
            var isLoggedIn = jQuery('.authorization-link > a').attr('href').indexOf('/login') < 0;
            if (isLoggedIn) {
                jQuery("body").addClass("customer-logged_in");
            }
        }
        //mobile navigation last section
        jQuery(".last-navigation .opener").click(function() {
            jQuery("ul.sub-naigationblock").slideToggle();
        });

    });

    //mobile navigation close button
    jQuery('.nav-close-trigger a').on("click", function(event) {
        jQuery("html").removeClass("nav-before-open");
        jQuery("html").removeClass("showNav");
        jQuery("html").removeClass("nav-open");
        event.stopPropagation();
    });

    var closed = false;
    var executeOnce = false;

    //set cookie.
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    //get cookie.
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //check cookie. returns boolean
    function checkCookie(cname) {
        var user = getCookie(cname);
        if (user != "") {
            return "1";
        } else {
            return "0";
        }
    }

    function addCloseButtonOnTopNav() {

        if ((jQuery(window).width() > 1199) && (jQuery(".header,panel .header.links .temp_popup_dropdown_content").length)) {

            //store switcher popup based on cookie.
            var popupStoreswitcherShow = checkCookie("storeswitcher_popup_show");
            if (popupStoreswitcherShow != "1") {
                jQuery("body").addClass("temp_popup_dropdown_content-open");
            }

            jQuery(".temp_popup_dropdown_content button").click(function() {
                if (jQuery(this).hasClass('cancel')) {
                    //set cookie with value
                    setCookie("storeswitcher_popup_show", 1, 1);
                    jQuery("body").removeClass("temp_popup_dropdown_content-open");
                } else {
                    //set cookie with value
                    setCookie("storeswitcher_popup_show", 1, 1);
                    jQuery("body").removeClass("temp_popup_dropdown_content-open");
                    var redirectToCorporate = jQuery(this).data('href');
                    window.location.href = redirectToCorporate;
                }

            });
        }
        if (jQuery(window).width() < 1200) {

            // if (!executeOnce) {
            //     executeOnce = true;
            // jQuery(".top-categories-list > h4").click(function() {
            //     jQuery(this).siblings(".sub-cat-ul").slideToggle();
            // });

            // jQuery(".left-tabs-wrap h4").click(function() {
            //     jQuery(this).siblings(".nav-tabs").slideToggle();
            // });

            // jQuery(".catalog-category-view .title,.catalog-category-view dt").click(function() {
            //     jQuery(".sidebar-main .items").slideToggle();
            // });

            // jQuery(".catalog-category-view .filter-title").click(function() {
            //     jQuery(".sidebar-main .filter-content").slideToggle();
            // });

            // }

            if (!(jQuery(".corporate-panel").length)) {

                if (!closed) {
                    var ph = jQuery(".header.panel .contactus-link a strong").text();
                    jQuery(".header.panel").prepend(jQuery(".header.panel .contactus-link").clone());
                    jQuery(".header.panel .contactus-link a").text(ph);
                }

                // jQuery(".header.panel .header.links>ul").append("<div class='close-sec'><button class='close-btn'>X Close</button></div>");
                jQuery(".header.panel .header.links>ul").addClass("corporate-panel").prependTo(".page-header > .panel.wrapper");
            }
            // jQuery(".corporate-panel .close-btn").on("click", function () {
            //     jQuery(".corporate-panel").remove();
            //     closed = true;
            // });
        } else {
            jQuery(".header.panel .header.links > .contactus-link").remove();
            executeOnce = false;
        }

        if (jQuery(window).width() < 1023) {

            jQuery(".catalog-category-view .sidebar-main .filter .block-subtitle").click(function() {
                jQuery(".sidebar-main .filter-options").slideToggle();
            });


        }
        jQuery(".corporate-panel .close-btn").on("click", function() {
            jQuery(".corporate-panel").remove();
        });
    }

    addCloseButtonOnTopNav();

    jQuery(window).resize(function() {
        addCloseButtonOnTopNav();
    });

    jQuery(document).on('click', '.qty button', function() {
        var qty = jQuery('#qty').val();
        var cartPageQty = jQuery(this).siblings(".input-text").val();
        if (jQuery(this).hasClass('sub')) {
            qty--;
            cartPageQty--;
        }
        if (jQuery(this).hasClass('add')) {
            qty++;
            cartPageQty++;
        }
        jQuery('#qty').val(qty);
        jQuery(this).siblings(".input-text").val(cartPageQty)
    });

    // detail page read more 
    // var maxLength = 100;
    // jQuery(".catalog-product-view .product-info-main .product.attribute.overview p").each(function () {
    //     var myStr = jQuery(this).text();
    //     if (jQuery.trim(myStr).length > maxLength) {
    //         var newStr = myStr.substring(0, maxLength);
    //         var removedStr = myStr.substring(maxLength, jQuery.trim(myStr).length);
    //         jQuery(this).empty().html(newStr);
    //         jQuery(this).append(' <a href="javascript:void(0);" class="read-more">...<span>Read More</span></a>');
    //         jQuery(this).append('<span class="more-text">' + removedStr + '</span>');
    //     }
    // });
    if (jQuery('.catalog-product-view .product-info-main .product.attribute.overview p').length) {
        var element = document.querySelector('.catalog-product-view .product-info-main .product.attribute.overview p');
        if ((element.offsetHeight < element.scrollHeight) || (element.offsetWidth < element.scrollWidth)) {
            // your element have overflow
            jQuery(".catalog-product-view .product-info-main .product.attribute.overview p").append(' <a href="javascript:void(0);" class="read-more">...<span>Read More</span></a>');
        }
    }


    jQuery(".read-more").click(function() {
        // jQuery(this).siblings(".more-text").contents().unwrap();
        jQuery(this).parent().css({
            "overflow": "visible",
            "-webkit-line-clamp": "unset"
        });
        jQuery(this).remove();
    });

    function actionsliderblock() {
        if (jQuery(window).width() < 1200) {
            jQuery('.callbanners-outercontainer').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 1050,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
    }


    /*sift content*/
    function shiftContent() {
        if (jQuery(".nav-toggle").is(":visible")) {
            jQuery(".page-main").each(function(index) {
                jQuery(".row.callbanners-outercontainer.shifting-contentblk").insertAfter('.row.on-sale');
            });
        } else {
            jQuery(".page-main").each(function(index) {
                jQuery(".row.callbanners-outercontainer.shifting-contentblk").insertBefore('.row.on-sale');
            });
        }
    }

    /*sift content*/
    jQuery(window).ready(function() {

        jQuery('.product-items.widget-product-grid').slick({
            infinite: false,
            slidesToShow: 6.5,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 5.5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 4.5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3.5,
                        arrows: false,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2.5,
                        arrows: false,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1.5,
                        arrows: false,
                        slidesToScroll: 1
                    }
                }
            ]

        });

        actionsliderblock();
        shiftContent();

    });

    jQuery(window).resize(function() {
        // actionsliderblock();
        shiftContent();
    });



    // mobile navbar submenu scroll close menu:  issue fix 
    // var moveX, moveY;
    // jQuery('#mainNav').on('touchstart', function(e) {
    //     moveX = e.originalEvent.changedTouches[0].pageX
    //     moveY = e.originalEvent.changedTouches[0].pageY

    // });
    // jQuery('#mainNav').on('touchmove', function(e) {
    //     var deltaX = e.originalEvent.changedTouches[0].pageX - moveX;
    //     var deltaY = e.originalEvent.changedTouches[0].pageY - moveY;
    //     var difference = (deltaX * deltaX) + (deltaY * deltaY);
    //     if (Math.sqrt(difference) > 1) {
    //         e.preventDefault();
    //     }
    // });
});