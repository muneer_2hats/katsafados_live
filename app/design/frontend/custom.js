require(['jquery', 'jquery/ui'], function (jQuery) {
    jQuery(document).ready(function () {

        /* navbar toggle */

        jQuery(".page-header .nav-toggle").click(function(){
            jQuery("html .sections.nav-sections").toggleClass("showNav");
        });


        /* navbar toggle ends */


        /* top panel dropdowns */
        jQuery(".authorization-link").mouseenter(function () {
            jQuery(".customer-welcome,.customer-name").toggleClass("active");
        });

        jQuery(".customer-welcome .customer-menu ul").mouseleave(function () {
            jQuery(".customer-welcome,.customer-name").toggleClass("active");
        });

        jQuery('.dropdown').hover(function () {
            jQuery('.dropdown-toggle', this).trigger('click');
        });

        jQuery("li.external-links").hover(function () {
            jQuery('.external-link-ul').css("display", "block");
        }, function () {
            jQuery(".external-link-ul").hover(function () {
                jQuery('.external-link-ul').css("display", "block");
            }, function () {
                jQuery(this).css("display", "none");
            });
        });

        /* top panel dropdowns ends */

        /* hover tab */

        jQuery('.nav-tabs a').hover(function(e){
            e.preventDefault();
            jQuery('.tab-pane').removeClass('active');
            tabContentSelector = jQuery(this).attr('href');
            jQuery(this).tab('show');
            jQuery(tabContentSelector).addClass('active');
        });
        /* hovertab ends */

        // custom toggle mobile navbar 

        

        //add class to body when customer logged in
        var isLoggedIn = jQuery('.authorization-link > a').attr('href').indexOf('/login') < 0;
        if (isLoggedIn) {
            jQuery("body").addClass("customer-logged_in");
        }
    });


    var closed = false;
    var executeOnce = false;

    //set cookie.
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    //get cookie.
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //check cookie. returns boolean
    function checkCookie(cname) {
        var user = getCookie(cname);
        if (user != "") {
            return "1";
        } else {
            return "0";
        }
    }

    function addCloseButtonOnTopNav() {

        if ((jQuery(window).width() > 1199) && (jQuery(".header,panel .header.links .temp_popup_dropdown_content").length)) {

            //store switcher popup based on cookie.
            var popupStoreswitcherShow = checkCookie("storeswitcher_popup_show");
            if (popupStoreswitcherShow != "1") {
                jQuery("body").addClass("temp_popup_dropdown_content-open");
            }

            jQuery(".temp_popup_dropdown_content button").click(function () {
                if (jQuery(this).hasClass('cancel')) {
                    //set cookie with value
                    setCookie("storeswitcher_popup_show", 1, 1);
                    jQuery("body").removeClass("temp_popup_dropdown_content-open");
                } else {
                    //set cookie with value
                    setCookie("storeswitcher_popup_show", 1, 1);
                    jQuery("body").removeClass("temp_popup_dropdown_content-open");
                    var redirectToCorporate = jQuery(this).data('href');
                    window.location.href = redirectToCorporate;
                }

            });
        }
        if (jQuery(window).width() < 1200) {

            if (!executeOnce) {
                executeOnce = true;
                jQuery(".top-categories-list > h4").click(function () {
                    jQuery(this).siblings(".sub-cat-ul").slideToggle();
                });

                jQuery(".left-tabs-wrap h4").click(function () {
                    jQuery(this).siblings(".nav-tabs").slideToggle();
                });

                jQuery(".catalog-category-view .title,.catalog-category-view dt").click(function () {
                    jQuery(".sidebar-main .items").slideToggle();
                });

                jQuery(".catalog-category-view .filter-title").click(function () {
                    jQuery(".sidebar-main .filter-content").slideToggle();
                });

            }

            if (!(jQuery(".header.panel .header.links>ul .close-btn").length)) {

                if (!closed) {
                    var ph = jQuery(".header.panel .contactus-link a").attr("href").match(/\d+/);
                    jQuery(".header.panel .header.links").append(jQuery(".header.panel .contactus-link").clone());
                    jQuery(".header.panel .contactus-link a").text(ph);
                }

                jQuery(".header.panel .header.links>ul").append("<div class='close-sec'><button class='close-btn'>X Close</button></div>");
            }
            jQuery(".header.panel .header.links>ul .close-btn").on("click", function () {
                jQuery(".header.panel .header.links>ul").remove();
                jQuery(".header.panel .header.links").addClass("closed");
                closed = true;
            });
        } else {
            jQuery(".header.panel .header.links > .contactus-link").remove();
            executeOnce = false;
        }

        if (jQuery(window).width() < 767) {

            jQuery(".catalog-category-view .title,.catalog-category-view dt").click(function () {
                jQuery(".sidebar-main .items").slideToggle();
            });

            jQuery(".catalog-category-view .sidebar-main .filter .block-subtitle").click(function () {
                jQuery(".sidebar-main .filter-options").slideToggle();
            });

            jQuery("html").addClass("nav-open");

        }
        jQuery(".header.panel .header.links>ul .close-btn").on("click", function () {
            jQuery(".header.panel .header.links>ul").remove();
        });
    }

    addCloseButtonOnTopNav();

    jQuery(window).resize(function () {
        addCloseButtonOnTopNav();
    });

    jQuery(document).on('click', '.qty button', function () {
        var qty = jQuery('#qty').val();
        var cartPageQty = jQuery(this).siblings(".input-text").val();
        if (jQuery(this).hasClass('sub')) {
            qty--;
            cartPageQty--;
        }
        if (jQuery(this).hasClass('add')) {
            qty++;
            cartPageQty++;
        }
        jQuery('#qty').val(qty);
        jQuery(this).siblings(".input-text").val(cartPageQty)
    });

    // detail page read more 
    var maxLength = 100;
    jQuery(".catalog-product-view .product-info-main .product.attribute.overview p").each(function () {
        var myStr = jQuery(this).text();
        if (jQuery.trim(myStr).length > maxLength) {
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, jQuery.trim(myStr).length);
            jQuery(this).empty().html(newStr);
            jQuery(this).append(' <a href="javascript:void(0);" class="read-more">...<span>Read More</span></a>');
            jQuery(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    jQuery(".read-more").click(function () {
        jQuery(this).siblings(".more-text").contents().unwrap();
        jQuery(this).remove();
    });

});
