<?php
namespace Hatslogic\Store\Plugin;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\View\Page\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Cache\Manager;

class FlushCache implements ObserverInterface
{
    
    protected $cacheManager;

    public function __construct(
        \Magento\Framework\App\Cache\Manager $cacheManager
    ){
        $this->cacheManager = $cacheManager;
    }
    public function execute(Observer $observer){
        $this->cacheManager->flush($this->cacheManager->getAvailableTypes());
    }
}