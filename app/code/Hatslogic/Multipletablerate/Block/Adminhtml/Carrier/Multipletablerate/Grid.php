<?php

namespace Hatslogic\Multipletablerate\Block\Adminhtml\Carrier\Multipletablerate;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Website filter
     *
     * @var int
     */
    protected $websiteId;

    /**
     * Condition filter
     *
     * @var string
     */
    protected $conditionName;

    /**
     * @var \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate
     */
    protected $matrixrate;

    /**
     * @var \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate\CollectionFactory $collectionFactory
     * @param \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate $matrixrate
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate\CollectionFactory $collectionFactory,
        \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate $matrixrate,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->matrixrate = $matrixrate;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Define grid properties
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('shippingMatrixrateGrid');
        $this->_exportPageSize = 10000;
    }

    /**
     * Set current website
     *
     * @param int $websiteId
     * @return $this
     */
    public function setWebsiteId($websiteId)
    {
        $this->websiteId = $this->_storeManager->getWebsite($websiteId)->getId();
        return $this;
    }

    /**
     * Retrieve current website id
     *
     * @return int
     */
    public function getWebsiteId()
    {
        if ($this->websiteId === null) {
            $this->websiteId = $this->_storeManager->getWebsite()->getId();
        }
        return $this->websiteId;
    }

    /**
     * Set current website
     *
     * @param string $name
     * @return $this
     */
    public function setConditionName($name)
    {
        $this->conditionName = $name;
        return $this;
    }

    /**
     * Retrieve current website id
     *
     * @return int
     */
    public function getConditionName()
    {
        return $this->conditionName;
    }

    /**
     * Prepare shipping table rate collection
     *
     * @return \Hatslogic\Multipletablerate\Block\Adminhtml\Carrier\Multipletablerate\Grid
     */
    protected function _prepareCollection()
    {
        /** @var $collection \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate\Collection */
        $collection = $this->collectionFactory->create();
        $collection->setConditionFilter($this->getConditionName())->setWebsiteFilter($this->getWebsiteId());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare table columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'dest_country',
            ['header' => __('Country'), 'index' => 'dest_country', 'default' => '*']
        );

        $this->addColumn(
            'dest_region',
            ['header' => __('Region/State'), 'index' => 'dest_region', 'default' => '*']
        );
        
        $this->addColumn(
            'dest_zip',
            ['header' => __('Zip/Postal Code'), 'index' => 'dest_zip', 'default' => '*']
        );

        $label = $this->matrixrate->getCode('condition_name_short', $this->getConditionName());

        $this->addColumn(
            'condition_value',
            ['header' => $label, 'index' => 'condition_value']
        );

        $this->addColumn('price', ['header' => __('Shipping Price'), 'index' => 'price']);

        $this->addColumn(
            'method_code',
            ['header' => __('Method Code'), 'index' => 'method_code']
        );

        $this->addColumn(
            'method_name',
            ['header' => __('Method Name'), 'index' => 'method_name']
        );

        $this->addColumn(
            'method_description',
            ['header' => __('Method Description'), 'index' => 'method_description']
        );

        $this->addColumn(
            'condition_type',
            ['header' => 'Condition Type', 'index' => 'condition_type']
        );

        return parent::_prepareColumns();
    }
}
