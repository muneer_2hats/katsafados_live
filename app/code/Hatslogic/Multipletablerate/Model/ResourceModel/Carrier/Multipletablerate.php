<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Hatslogic\Multipletablerate\Model\ResourceModel\Carrier;

use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Multipletablerate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Import table rates website ID
     *
     * @var int
     */
    protected $importWebsiteId = 0;

    /**
     * Errors in import process
     *
     * @var array
     */
    protected $importErrors = [];

    /**
     * Count of imported table rates
     *
     * @var int
     */
    protected $importedRows = 0;

    /**
     * Array of unique table rate keys to protect from duplicates
     *
     * @var array
     */
    protected $importUniqueHash = [];

    /**
     * Array of countries keyed by iso2 code
     *
     * @var array
     */
    protected $importIso2Countries;

    /**
     * Array of countries keyed by iso3 code
     *
     * @var array
     */
    protected $importIso3Countries;

    /**
     * Associative array of countries and regions
     * [country_id][region_code] = region_id
     *
     * @var array
     */
    protected $importRegions;

    /**
     * Import Table Rate condition name
     *
     * @var string
     */
    protected $importConditionName;

    /**
     * Array of condition full names
     *
     * @var array
     */
    protected $conditionFullNames = [];

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $coreConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate
     */
    protected $carrierMultipletablerate;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $countryCollectionFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     *   * @var \Magento\Framework\Filesystem\Directory\ReadFactory
     */
    private $readFactory;
    /**
     *   * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    private $resourceConnection;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $coreConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate $carrierMultipletablerate
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param string|null $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $coreConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate $carrierMultipletablerate,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory,
        \Magento\Framework\Filesystem $filesystem,
        ResourceConnection $resourceConnection,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->coreConfig = $coreConfig;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->carrierMultipletablerate = $carrierMultipletablerate;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->readFactory = $readFactory;
        $this->filesystem = $filesystem;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Define main table and id field name
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('hatslogic_multipletableartes', 'pk');
    }

    /**
     * Return table rate array or false by rate request
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @param bool $zipRangeSet
     * @return array|bool
     */
    public function getRate(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        $adapter = $this->resourceConnection->getConnection();
        $shippingData=[];
        
        
        $select = $adapter->select()->from(
            $this->getMainTable()
        );

        $select->where(
            $adapter->quoteInto(" (dest_country_id=? ", $request->getDestCountryId()) .
                $adapter->quoteInto(" AND dest_region_id=? ", $request->getDestRegionId()) .
                $adapter->quoteInto(" AND dest_zip=?) ", $request->getDestPostcode()) .

            $adapter->quoteInto(" OR (dest_country_id=? ", $request->getDestCountryId()) .
                $adapter->quoteInto(" AND dest_region_id=? AND dest_zip='') ", $request->getDestRegionId()) .

            $adapter->quoteInto(" OR (dest_country_id=? AND dest_region_id='0' AND dest_zip='') ", $request->getDestCountryId()) .

            $adapter->quoteInto(" OR (dest_country_id=? AND dest_region_id='0' ", $request->getDestCountryId()) .
                $adapter->quoteInto("  AND dest_zip=?) ", $request->getDestPostcode()) .

            " OR (dest_country_id='0' AND dest_region_id='0' AND dest_zip='')"
        );

        
        if (is_array($request->getConditionName())) {
            $i = 0;

            foreach ($request->getConditionName() as $conditionName) {
                if ($i == 0) {
                    $select->where('condition_name=?', $conditionName);
                } else {
                    $select->orWhere('condition_name=?', $conditionName);
                }

                $select->where('condition_value>=?', $request->getData($conditionName));
                $i++;
            }
        } else {
            $select->where('condition_name=?', $request->getConditionName());
            $select->where('condition_value>=?', $request->getData($request->getConditionName()));
        }

        $select->where('website_id=?', $request->getWebsiteId());

        $select->group('method_code');
        $select->group('dest_zip');
        $select->group('dest_region_id');
        $select->group('dest_country_id');

        $select->order('dest_zip DESC');
        $select->order('dest_region_id DESC');
        $select->order('dest_country_id DESC');

        $select->order('condition_value ASC');
        
        //echo $select->__toString();exit;
        #Mage::log($select->__toString());
        

        $rows = $adapter->fetchAll($select);
        //print_r($rows);exit;       
        /*
         * Check to see if any zip code or region specific rates exist. 
         */
        $specific = array();
        $failover = array();
        
        $zipFound = $regionFound = false;
        
        /*
         * Most specific: Zip code. 
         * Check for any not null zip codes, which would mean that we have a specific shipping method for that zipcode
         */
        foreach($rows as $row)
        {        	
        	if (!empty($row['dest_zip']))
        	{
        		$specific[] = $row;
        		$zipFound = true;
        	}
        	elseif (!empty($row['dest_region_id']) && $zipFound == false)
        	{
        		$specific[] = $row;
        		$regionFound = true;
        	}        	
        	elseif (!empty($row['dest_country_id']) && $zipFound == false && $regionFound == false)
        	{
        		$specific[] = $row;
        	}

			if (empty($row['dest_country_id']) && empty($row['dest_region_id']) && empty($row['dest_zip']))
			{
				$failover[] = $row;
			}			
        }
        
        if (count($specific) > 0)
        {
        	$rates = $specific;
        }
        else
        {
        	$rates = $failover;
        }
        return $rates;
    }
    /**
     * Upload table rate file and import data from it
     *
     * @param \Magento\Framework\Object $object
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function uploadAndImport(\Magento\Framework\DataObject $object)
    {
        //M2-24
        $importFieldData = $object->getFieldsetDataValue('import');
        if (empty($importFieldData['tmp_name'])) {
            return $this;
        }

        $website = $this->storeManager->getWebsite($object->getScopeId());
        $csvFile = $importFieldData['tmp_name'];

        $this->importWebsiteId = (int)$website->getId();
        $this->importUniqueHash = [];
        $this->importErrors = [];
        $this->importedRows = 0;

        //M2-20
        $tmpDirectory = ini_get('upload_tmp_dir')? $this->readFactory->create(ini_get('upload_tmp_dir'))
            : $this->filesystem->getDirectoryRead(DirectoryList::SYS_TMP);
        $path = $tmpDirectory->getRelativePath($csvFile);
        $stream = $tmpDirectory->openFile($path);

        // check and skip headers
        $headers = $stream->readCsv();
        if ($headers === false || count($headers) < 9) {
            $stream->close();
            throw new \Magento\Framework\Exception\LocalizedException(__('Please correct Matrix Rates File Format.'));
        }

        if ($object->getData('groups/multipletablerate/fields/condition_name/inherit') == '1') {
            $conditionName = (string)$this->coreConfig->getValue('carriers/multipletablerate/condition_name', 'default');
        } else {
            $conditionName = $object->getData('groups/multipletablerate/fields/condition_name/value');
        }
        $this->importConditionName = $conditionName;

        $adapter = $this->getConnection();
        $adapter->beginTransaction();

        try {
            $rowNumber = 1;
            $importData = [];

            $this->_loadDirectoryCountries();
            $this->_loadDirectoryRegions();

            // delete old data by website and condition name
            $condition = [
                'website_id = ?' => $this->importWebsiteId,
                'condition_name = ?' => $this->importConditionName,
            ];
            $adapter->delete($this->getMainTable(), $condition);

            while (false !== ($csvLine = $stream->readCsv())) {
                $rowNumber++;

                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);
                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = [];
                }
            }
            $this->_saveImportData($importData);
            $stream->close();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $adapter->rollback();
            $stream->close();
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        } catch (\Exception $e) {
            $adapter->rollback();
            $stream->close();
            $this->logger->critical($e);
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while importing matrix rates.')
            );
        }

        $adapter->commit();

        if ($this->importErrors) {
            $error = __(
                'We couldn\'t import this file because of these errors: %1',
                implode(" \n", $this->importErrors)
            );
            throw new \Magento\Framework\Exception\LocalizedException($error);
        }

        return $this;
    }

    /**
     * Load directory countries
     *
     * @return \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate
     */
    protected function _loadDirectoryCountries()
    {
        if ($this->importIso2Countries !== null && $this->importIso3Countries !== null) {
            return $this;
        }

        $this->importIso2Countries = [];
        $this->importIso3Countries = [];

        /** @var $collection \Magento\Directory\Model\ResourceModel\Country\Collection */
        $collection = $this->countryCollectionFactory->create();
        foreach ($collection->getData() as $row) {
            $this->importIso2Countries[$row['iso2_code']] = $row['country_id'];
            $this->importIso3Countries[$row['iso3_code']] = $row['country_id'];
        }

        return $this;
    }

    /**
     * Load directory regions
     *
     * @return \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate
     */
    protected function _loadDirectoryRegions()
    {
        if ($this->importRegions !== null) {
            return $this;
        }

        $this->importRegions = [];

        /** @var $collection \Magento\Directory\Model\ResourceModel\Region\Collection */
        $collection = $this->regionCollectionFactory->create();
        foreach ($collection->getData() as $row) {
            $this->importRegions[$row['country_id']][$row['code']] = (int)$row['region_id'];
        }

        return $this;
    }

    /**
     * Return import condition full name by condition name code
     *
     * @param string $conditionName
     * @return string
     */
    protected function getConditionFullName($conditionName)
    {
        if (!isset($this->conditionFullNames[$conditionName])) {
            $name = $this->carrierMultipletablerate->getCode('condition_name_short', $conditionName);
            $this->conditionFullNames[$conditionName] = $name;
        }

        return $this->conditionFullNames[$conditionName];
    }

    /**
     * Validate row for import and return table rate array or false
     * Error will be add to importErrors array
     *
     * @param array $row
     * @param int $rowNumber
     * @return array|false
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _getImportRow($row, $rowNumber = 0)
    {
       
        // validate row
        if (count($row) < 9) {
            $this->importErrors[] =
                __('Please correct Matrix Rates format in Row #%1. Invalid Number of Rows', $rowNumber);
            return false;
        }

        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }

        // validate country
        if (isset($this->importIso2Countries[$row[0]])) {
            $countryId = $this->importIso2Countries[$row[0]];
        } elseif (isset($this->importIso3Countries[$row[0]])) {
            $countryId = $this->importIso3Countries[$row[0]];
        } elseif ($row[0] == '*' || $row[0] == '') {
            $countryId = '0';
        } else {
            $this->importErrors[] = __('Please correct Country "%1" in Row #%2.', $row[0], $rowNumber);
            return false;
        }

        // validate region
        if ($countryId != '0' && isset($this->importRegions[$countryId][$row[1]])) {
            $regionId = $this->importRegions[$countryId][$row[1]];
        } elseif ($row[1] == '*' || $row[1] == '') {
            $regionId = 0;
        } else {
            $this->importErrors[] = __('Please correct Region/State "%1" in Row #%2.', $row[1], $rowNumber);
            return false;
        }

        // detect zip code
        if ($row[2] == '*' || $row[2] == '') {
            $zip = '';
        } else {
            $zip = $row[2];
        }


        // detect Weight (and above)
        $condition_value = $this->_parseDecimalValue($row[3]);
        if ($condition_value === false) {

            $this->importErrors[] = __(
                'Please correct %1 From "%2" in Row #%3.',
                $this->getConditionFullName($this->importConditionName),
                $row[3],
                $rowNumber
            );
            return false;
        }

        //print_r($this->importErrors);exit;
        //validate shipping price
        $price = $this->_parseDecimalValue($row[4]);
        if ($price === false) {
            $this->importErrors[] = __('Please correct Shipping Price "%1" in Row #%2.', $row[4], $rowNumber);
            return false;
        }

        //validate shipping method code
        if ($row[5] == '*' || $row[5] == '') {
            $this->importErrors[] = __('Please correct Shipping Method Code "%1" in Row #%2.', $row[5], $rowNumber);
            return false;
        } else {
            $shippingMethodCode = $row[5];
        }

        //validate shipping method name
        if ($row[6] == '*' || $row[6] == '') {
            $this->importErrors[] = __('Please correct Shipping Method Name "%1" in Row #%2.', $row[6], $rowNumber);
            return false;
        } else {
            $shippingMethodName = $row[6];
        }

        //validate shipping method description
        $shippingMethodDescription = $row[7];

        
        
        /* 
        * Column 6 - Method Code
        */
        $method_code = strtolower($row[5]);
        
        /*
        * Column 7 - Method Name
        */
        $method_name = $row[6];
        
        /*
        * Column 8 - Method Description
        */
        $method_description = $row[7];
        
        /*
        * Column 9 - Condition type ("percent" or "value")
        */
        $condition_type = $row[8];

        

        return [
            $this->importWebsiteId,    // website_id
            $countryId,                 // dest_country_id
            $regionId, 
            $zip,                 // dest_region_id,
            $this->importConditionName,// condition_name,
            $condition_value,
            $condition_type,
            $method_code,
            $method_name,
            $method_description,
            $price
        ];
    }

    /**
     * Save import data batch
     *
     * @param array $data
     * @return \Hatslogic\Multipletablerate\Model\ResourceModel\Carrier\Multipletablerate
     */
    protected function _saveImportData(array $data)
    {
        
        if (!empty($data)) {
            $columns = [
                'website_id',
                'dest_country_id',
                'dest_region_id',
                'dest_zip',
                'condition_name',
                'condition_value',
                'condition_type',
                'method_code',
                'method_name',
                'method_description',
                'price'
            ];
            $this->getConnection()->insertArray($this->getMainTable(), $columns, $data);
            $this->importedRows += count($data);
        }

        return $this;
    }

    /**
     * Parse and validate positive decimal value
     * Return false if value is not decimal or is not positive
     *
     * @param string $value
     * @return bool|float
     */
    protected function _parseDecimalValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        $value = (double)sprintf('%.4F', $value);
        if ($value < 0.0000) {
            return false;
        }
        return $value;
    }
}
