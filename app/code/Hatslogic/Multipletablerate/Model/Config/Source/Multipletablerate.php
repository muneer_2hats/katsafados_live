<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Hatslogic\Multipletablerate\Model\Config\Source;

class Multipletablerate implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate
     */
    protected $carrierMultipletablerate;

    /**
     * @param \Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate $carrierMultipletablerate
     */
    public function __construct(\Hatslogic\Multipletablerate\Model\Carrier\Multipletablerate $carrierMultipletablerate)
    {
        $this->carrierMultipletablerate = $carrierMultipletablerate;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $arr = [];
        foreach ($this->carrierMultipletablerate->getCode('condition_name') as $k => $v) {
            $arr[] = ['value' => $k, 'label' => $v];
        }
        return $arr;
    }
}
