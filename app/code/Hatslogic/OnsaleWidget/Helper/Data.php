<?php 

namespace Hatslogic\OnsaleWidget\Helper;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Data extends \Magento\Framework\Url\Helper\Data
{

    /**
     * @var TimezoneInterface
     */
    protected $localeDate;
    
    protected $productFactory;

    public function __construct(
        TimezoneInterface $localeDate,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
        $this->localeDate = $localeDate;
    }

    public function isProductOnSale($product)
    {
        $_product = $this->productFactory->create()->load($product->getId());
        $orgprice = $_product->getPrice();
        $specialprice = $_product->getSpecialPrice();
        $specialfromdate = $_product->getSpecialFromDate();
        $specialtodate = $_product->getSpecialToDate();
        $today = time();
        if ($specialprice) {
            if ($specialprice< $orgprice) {
                if ((is_null($specialfromdate) &&is_null($specialtodate)) || ($today >= strtotime($specialfromdate) &&is_null($specialtodate)) || ($today <= strtotime($specialtodate) &&is_null($specialfromdate)) || ($today >= strtotime($specialfromdate) && $today <= strtotime($specialtodate))) {
                    $save_percent = 100-round(($specialprice/$orgprice)*100);
                    return $save_percent;
                }
            }
        }
        
        return 0;
    }
}   