<?php

use Magento\Framework\App\Bootstrap;
ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
include 'app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
$id = 8350;
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

// $product = $objectManager->create('Magento\Catalog\Model\Product')->load($id);
// print_r($product->getMediaGallery('images'));
// die;

$productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
$productcollection = $productCollectionFactory->create()
                        ->addAttributeToSelect('*')
                        ->load();
$products_array = [];                        
foreach ($productcollection as $product) {
    $productId = $product->getId();
    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
    // echo $productId;
    // echo $product->getThumbnail();
    // die;
    if(empty($product->getMediaGallery('images'))){
        $products_array[] = array(
         $product->getId(),
         $product->getSku(),
         $product->getName(),
        );
    }
}

$f = fopen("products_noimage.csv", "w");
foreach ($products_array as $line) {
    fputcsv($f, $line);
}

echo '<pre>';
print_r($products_array);