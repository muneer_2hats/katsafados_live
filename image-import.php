<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';

importImages();

function importImages(){

	$params = $_SERVER;
	 
	$bootstrap = Bootstrap::create(BP, $params);
	 
	$obj = $bootstrap->getObjectManager();
	 
	$state = $obj->get('Magento\Framework\App\State');
	$state->setAreaCode('frontend');


	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
	$dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
	$file = $objectManager->get('Magento\Framework\Filesystem\Io\File');

	
	$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
	$collection = $productCollection->create()
            ->addAttributeToSelect('*')
            ->setPageSize(10000)
            ->load();

    $importedData = file_get_contents('imageImportLog.txt');
	$importedSku = explode(',',$importedData);

	$str = file_get_contents('mage-1-products-gallery.json');
	$json = json_decode($str, true);
	$i=0; 
    foreach($collection  as $product) { 
		

		
    	if(isset($json[$product->getSku()]) && !empty($json[$product->getSku()]) && !in_array($product->getSku(), $importedSku))
    	{ $i++;
		    	echo $i.' : '.$product->getSku().'<br/>';
			$imageData = $json[$product->getSku()];
			
			uploadImages($imageData['image'], $product , $dir, $file, 1);

			if(!empty($imageData['gallery']))
			{
			 	foreach($imageData['gallery'] as $url) {
			 		if($imageData['image'] !=  $url) {
			 			uploadImages($url, $product, $dir, $file, 0); 
			 		}
			 	}
			}
			writeToLog($product->getSku());
		}
	}

	echo 'All Product images are imported : '. $i;

	$logFile = fopen("finalLog.txt", "a") or die("Unable to open file!");
	$log = 'All Product images are imported : '. $i."\n";
	fwrite($logFile, $log);
	fclose($logFile);
	return true;
}



function uploadImages($imageUrl , $product, $dir, $file, $IsMainImage = 0)
{ 
	$tmpDir = $dir->getPath('media').DIRECTORY_SEPARATOR.'tmp/';
	$file->checkAndCreateFolder($tmpDir);
	$newFileName = $tmpDir.baseName($imageUrl);
	$result = $file->read($imageUrl, $newFileName);
	
	if($result) {
		$visible = false;
		$imageType = [];
		if($IsMainImage) {
			$imageType = array ('thumbnail','small_image','image');
		}
		$product->addImageToMediaGallery($newFileName, $imageType, true, $visible);
		$product->save();
		echo "Importing";
	}
	return true;
}

function writeToLog($sku = '')
{
	$logFile = fopen("imageImportLog.txt", "a") or die("Unable to open file!");
	$log = $sku.",";
	fwrite($logFile, $log);
	fclose($logFile);
	return true;
}
