<?php

use Magento\Framework\App\Bootstrap;

include 'app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
$productcollection = $productCollectionFactory->create()
                        ->addAttributeToSelect('*')
                        ->load();

foreach ($productcollection as $product) {
    $productId = $product->getId();
    echo $productId;
    echo $product->getThumbnail();
    die;
}